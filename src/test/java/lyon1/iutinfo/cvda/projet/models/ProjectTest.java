/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.cvda.projet.models;

import java.util.ArrayList;
import lyon1.iutinfo.cvda.exceptions.ProjectIdException;
import lyon1.iutinfo.cvda.exceptions.ProjectCommitNumberException;
import lyon1.iutinfo.cvda.exceptions.ProjectWebURLException;
import org.apache.commons.codec.digest.DigestUtils;
import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA_224;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lowic
 */
public class ProjectTest
	{
	// Attributes declaration
	private Member m;
	private ArrayList<Member> memberList;
	private Project p;
	private ArrayList<Project> projectList;

	public ProjectTest ()
		{
		}

	@BeforeClass
	public static void setUpClass ()
		{
		}

	@AfterClass
	public static void tearDownClass ()
		{
		}

	// Attributes initialization ; these objects will be used in the tests
	@Before
	public void setUp ()
		{
		m = new Member(1, "Jean Pichon", "jean.dupont@gmail.com", "www.google.fr");
		memberList = new ArrayList();
		memberList.add(m);
		p = new Project(1, "CVDA", "www.google.fr", "www.forge.univ-lyon1.fr", 0, memberList);
		projectList = new ArrayList();
		projectList.add(p);
		m.setProjectList(projectList);
		}

	@After
	public void tearDown ()
		{
		}

	// Test of getId method, of class Project.
	@Test
	public void testGetId ()
		{
		int expResult = 1;
		int result = p.getId();
		assertEquals(expResult, result);
		}

	// Test of setId method, of class Project.
	@Test
	public void testSetId ()
		{
		try
			{
			int id = 2;
			p.setId(id);
			assertEquals(id, p.getId());
			}
		catch (ProjectIdException e)
			{
			fail();
			}
		}

	// Test of setId method, of class Project.
	@Test(expected = ProjectIdException.class)
	public void testSetIdException () throws ProjectIdException
		{
		try
			{
			int id = -1;
			p.setId(id);
			assertEquals(id, p.getId());
			}
		catch (ProjectIdException e)
			{
			throw new ProjectIdException();
			}
		}

	// Test of getName method, of class Project.
	@Test
	public void testGetName ()
		{
		String expResult = "CVDA";
		String result = p.getName();
		assertEquals(expResult, result);
		}

	// Test of setName method, of class Project.
	@Test
	public void testSetName ()
		{
		String name = "Jeu";
		p.setName(name);
		assertEquals(name, p.getName());
		}

	// Test of getWebURL method, of class Project.
	@Test
	public void testGetWebURL ()
		{
		String expResult = "www.google.fr";
		String result = p.getWebURL();
		assertEquals(expResult, result);
		}

	// Test of setWebURL method, of class Project.
	@Test
	public void testSetWebURL ()
		{
		try
			{
			String webURL = "https://www.git";
			p.setWebURL(webURL);
			assertEquals(webURL, p.getWebURL());
			}
		catch (ProjectWebURLException e)
			{
			fail();
			}
		}

	// Test of setWebURL method, of class Project.

	@Test(expected = ProjectWebURLException.class)
	public void testSetWebURLException () throws ProjectWebURLException
		{
		try
			{
			String webURL = "www.google.com";
			p.setWebURL(webURL);
			assertEquals(webURL, p.getWebURL());
			}
		catch (ProjectWebURLException e)
			{
			throw new ProjectWebURLException();
			}
		}

	// Test of getSshURL method, of class Project.

	@Test
	public void testGetSshURL ()
		{
		String expResult = "www.forge.univ-lyon1.fr";
		String result = p.getSshURL();
		assertEquals(expResult, result);
		}

	// Test of setSshURL method, of class Project.
	@Test
	public void testSetSshURL ()
		{
		String sshURL = "";
		p.setSshURL(sshURL);
		assertEquals(sshURL, p.getSshURL());
		}

	// Test of getLstMembres method, of class Project.
	@Test
	public void testGetMemberList ()
		{
		ArrayList<Member> expResult = memberList;
		ArrayList<Member> result = p.getMemberList();
		assertEquals(expResult, result);
		}

	// Test of setLstMembres method, of class Project.
	@Test
	public void testSetMemberList ()
		{
		ArrayList<Member> list = new ArrayList();
		p.setMemberList(list);
		assertEquals(list, p.getMemberList());
		}

	// Test of getNbCommits method, of class Project.
	@Test
	public void testGetCommitNumb ()
		{
		int expResult = 0;
		int result = p.getCommitNumb();
		assertEquals(expResult, result);
		}

	// Test of setNbCommits method, of class Project.
	@Test
	public void testSetCommitNumb ()
		{
		try
			{
			int commit_Numb = 2;
			p.setCommitNumb(commit_Numb);
			assertEquals(commit_Numb, p.getCommitNumb());
			}
		catch (ProjectCommitNumberException e)
			{
			fail();
			}
		}

	// Test of setNbCommits method, of class Project.
	@Test(expected = ProjectCommitNumberException.class)
	public void testSetCommitNumbException () throws ProjectCommitNumberException
		{
		try
			{
			int nbCommits = -1;
			p.setCommitNumb(nbCommits);
			assertEquals(nbCommits, p.getCommitNumb());
			}
		catch (ProjectCommitNumberException e)
			{
			throw new ProjectCommitNumberException();
			}
		}

	// Test of toString method, of class Project.
	@Test
	public void testToString ()
		{
		String expResult = "Projet id #1";
		expResult += "\nNom : \"CVDA\"";
		expResult += "\nsshURL : \"www.forge.univ-lyon1.fr\"";
		expResult += "\nwebURL : \"www.google.fr\"";
		expResult += "\nCommits : 0";
		expResult += "\nMembres (1)";
		expResult += "\n\t#1: Jean Pichon (1 projets)";
		String result = p.toString();
		System.out.println(p.toString());
		assertEquals(expResult, result);
		}

	// Test of toXML method, of class Project.
	@Test
	public void testToXML ()
		{
		String expString = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>";
		expString += "<projet id=\"1\" nbcommits=\"0\">";
		expString += "<nom>CVDA</nom>";
		expString += "<webURL>www.google.fr</webURL>";
		expString += "<sshURL>www.forge.univ-lyon1.fr</sshURL>";
		expString += "<membres>";
		expString += "<membre id=\"1\">";
		expString += "<nom>Jean Pichon</nom>";
		expString += "<nb-projets>1</nb-projets>";
		expString += "</membre>";
		expString += "</membres>";
		expString += "</projet>";
		String expResult = new DigestUtils(SHA_224).digestAsHex(expString);
		String string = p.toXML();
		String result = new DigestUtils(SHA_224).digestAsHex(string);
		assertEquals(expResult, result);
		}
	}
