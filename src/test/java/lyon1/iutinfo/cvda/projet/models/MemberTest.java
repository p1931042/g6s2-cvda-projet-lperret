/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.cvda.projet.models;

import java.util.ArrayList;
import lyon1.iutinfo.cvda.exceptions.MemberEmailException;
import lyon1.iutinfo.cvda.exceptions.MemberIdException;
import lyon1.iutinfo.cvda.exceptions.MemberNameException;
import lyon1.iutinfo.cvda.exceptions.MemberWebURLException;
import org.apache.commons.codec.digest.DigestUtils;
import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA_224;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gaelle bouchenot
 */
public class MemberTest
	{

	public MemberTest ()
		{
		}

	@BeforeClass
	public static void setUpClass ()
		{
		}

	@AfterClass
	public static void tearDownClass ()
		{
		}

	@Before
	public void setUp ()
		{
		}

	@After
	public void tearDown ()
		{
		}

	// Test of getId method, of class Member.
	@Test
	public void testGetId ()
		{
		System.out.println("getId");
		Member instance = new Member(1, "Dupont", "gmail", "web");
		int expResult = 1;
		int result = instance.getId();
		assertEquals(expResult, result);
		}

	// Test of setId method, of class Member.
	@Test
	public void testSetId ()
		{
		try
			{
			System.out.println("setId");
			int id = 1;
			Member instance = new Member(1, "Dupont", "gmail", "web");
			instance.setId(id);
			assertEquals(id, instance.getId());
			}
		catch (MemberIdException e)
			{
			fail();
			}
		}

	// Test of setId method, of class Member.
	@Test(expected = MemberIdException.class)
	public void testSetIdException () throws MemberIdException
		{
		try
			{
			System.out.println("setId");
			int id = -1;
			Member instance = new Member(1, "Dupont", "gmail", "web");
			instance.setId(id);
			assertEquals(id, instance.getId());
			}
		catch (MemberIdException e)
			{
			throw new MemberIdException();
			}
		}

	// Test of getName method, of class Member.
	@Test
	public void testGetNom ()
		{
		System.out.println("getNom");
		Member instance = new Member(1, "Dupont", "gmail", "web");
		String expResult = "Dupont";
		String result = instance.getName();
		assertEquals(expResult, result);
		}

	// Test of setName method, of class Member.
	@Test
	public void testSetName ()
		{
		try
			{
			System.out.println("setNom");
			String name = "test1";
			Member instance = new Member(1, "Dupont", "gmail", "web");
			instance.setName(name);
			assertEquals(name, instance.getName());
			}
		catch (MemberNameException e)
			{
			fail();
			}
		}

	// Test of setName method, of class Member.
	@Test(expected = MemberNameException.class)
	public void testSetNameException () throws MemberNameException
		{
		try
			{
			System.out.println("setNom");
			String name = "A";
			Member instance = new Member(1, "Dupont", "gmail", "web");
			instance.setName(name);
			assertEquals(name, instance.getName());
			}
		catch (MemberNameException e)
			{
			throw new MemberNameException();
			}
		}

	// Test of getEmail method, of class Member.
	@Test
	public void testGetEmail ()
		{
		System.out.println("getEmail");
		Member instance = new Member(1, "Dupont", "gmail", "web");
		String expResult = "gmail";
		String result = instance.getEmail();
		assertEquals(expResult, result);
		}

	// Test of setEmail method, of class Member.
	@Test
	public void testSetEmail ()
		{
		try
			{
			System.out.println("setEmail");
			String name = "test@gmail.com";
			Member instance = new Member(1, "Dupont", "gmail", "web");
			instance.setEmail(name);
			assertEquals(name, instance.getEmail());
			}
		catch (MemberEmailException e)
			{
			fail();
			}
		}

	// Test of setEmail method, of class Member.
	@Test(expected = MemberEmailException.class)
	public void testSetEmailException () throws MemberEmailException
		{
		try
			{
			System.out.println("setEmail");
			String name = "test.com";
			Member instance = new Member(1, "Dupont", "gmail", "web");
			instance.setEmail(name);
			assertEquals(name, instance.getEmail());
			}
		catch (MemberEmailException e)
			{
			throw new MemberEmailException();
			}
		}

	// Test of getWebsite method, of class Member.
	@Test
	public void testGetWebsite ()
		{
		System.out.println("getWebsite");
		Member instance = new Member(1, "Dupont", "gmail", "web");
		String expResult = "web";
		String result = instance.getWebsite();
		assertEquals(expResult, result);
		}

	// Test of setWebsite method, of class Member.
	@Test
	public void testSetWebsite ()
		{
		try
			{
			System.out.println("setWebsite");
			String name = "https://website.git";
			Member instance = new Member(1, "Dupont", "gmail", "web");
			instance.setWebsite(name);
			assertEquals(name, instance.getWebsite());
			}
		catch (MemberWebURLException e)
			{
			fail();
			}
		}

	// Test of setWebsite method, of class Member.
	@Test(expected = MemberWebURLException.class)
	public void testSetWebsiteException () throws MemberWebURLException
		{
		try
			{
			System.out.println("setWebsite");
			String name = "website.com";
			Member instance = new Member(1, "Dupont", "gmail", "web");
			instance.setWebsite(name);
			assertEquals(name, instance.getWebsite());
			}
		catch (MemberWebURLException e)
			{
			throw new MemberWebURLException();
			}
		}

	// Test of getprojectListets method, of class Member.
	@Test
	public void testGetProjectListe ()
		{
		System.out.println("getprojectListets");
		Member instance = new Member(1, "Dupont", "gmail", "web");
		ArrayList expResult = new ArrayList();
		ArrayList result = instance.getProjectList();
		assertEquals(expResult, result);
		}

	// Test of setprojectListets method, of class Member.
	@Test
	public void testSetProjectListe ()
		{
		System.out.println("setprojectListets");
		Project proj = new Project(1, "Dupont", "web", "ssh", 2);
		ArrayList projectList = new ArrayList();
		projectList.add(proj);
		Member instance = new Member(1, "Dupont", "gmail", "web");
		instance.setProjectList(projectList);
		assertEquals(projectList, instance.getProjectList());
		}

	// Test of toString method, of class Member.
	@Test
	public void testToString ()
		{
		System.out.println("toString");
		Member instance = new Member(1, "Dupont", "gmail", "web");
		String expResult = "";
		expResult += "Nom : Dupont";
		expResult += "Projet (0)";
		String result = instance.toString();
		assertEquals(expResult, result);
		}

	// Test of toXML method, of class Member.
	@Test
	public void testToXML ()
		{
		System.out.println("toXML");
		String string = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>"
			  + "<membre id=\"1\">"
			  + "<nom>Bouchenot</nom>"
			  + "<email>gmail</email>"
			  + "<website/>"
			  + "<nb-projets>1</nb-projets>"
			  + "</membre>";
		String expResult = new DigestUtils(SHA_224).digestAsHex(string);
		Project proj = new Project(1, "G6S2", "web", "ssh", 2);
		ArrayList projectList = new ArrayList();
		projectList.add(proj);
		Member instance = new Member(1, "Bouchenot", "gmail", "", projectList);
		String result = new DigestUtils(SHA_224).digestAsHex(instance.toXML());
		assertEquals(expResult, result);
		}

	}
