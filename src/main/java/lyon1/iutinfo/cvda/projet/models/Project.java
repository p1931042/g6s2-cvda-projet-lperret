/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.cvda.projet.models;

import java.io.File;
import java.io.StringWriter;
import java.util.ArrayList;
import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import lyon1.iutinfo.cvda.exceptions.ProjectIdException;
import lyon1.iutinfo.cvda.exceptions.ProjectCommitNumberException;
import lyon1.iutinfo.cvda.exceptions.ProjectWebURLException;
import org.w3c.dom.Element;

/**
 * <b>Project is the Java class that defines a project on which members are working.</b>
 * 
 * <p>A project has the following attributes :<p>
 * <ul>
 * <li>An identifier</li>
 * <li>A name</li>
 * <li>A website URL</li>
 * <li>An SSH URL</li>
 * <li>A list of members working on the project</li>
 * <li>The number of commits</li>
 * </ul>
 * 
 * @author lowic
 * @version 1.0
 */
public class Project
	{
	/**
	 * The project ID. It can be modified.
	 */
	private int id;
	/**
	 * The project name. It can be modified.
	 */
	private String name;
	/**
	 * The project web URL. It can be modified.
	 */
	private String webURL;
	/**
	 * The project SSH URL. It can be modified.
	 */
	private String sshURL;
	/**
	 * The project member list. It can be modified.
	 * <p>It does not need to be initialised in the constructor.</p>
	 */
	private ArrayList<Member> memberList;
	/**
	 * The project commit number. It can be modified.
	 */
	private int commitNumb;

	/**
	 * The first constructor of a Project object.
	 * <p>The member list is not specified.</p>
	 * @param num
	 *	the id to set
	 * @param n
	 *	the name to set
	 * @param w
	 *	the website URL to set
	 * @param s
	 *	the SSH URL to set
	 * @param c
	 *	the number of commits to set
	 */
	public Project (int num, String n, String w, String s, int c)
		{
		id = num;
		name = n;
		webURL = w;
		sshURL = s;
		commitNumb = c;
		memberList = new ArrayList();
		}

	/**
	 * The second constructor of a Project object.
	 * <p>The member list is specified.</p>
	 * @param num
	 *	the id to set
	 * @param n
	 *	the name to set
	 * @param w
	 *	the website URL to set
	 * @param s
	 *	the SSH URL to set
	 * @param c
	 *	the number of commits to set
	 * @param l
	 *	the member list to set
	 */
	public Project (int num, String n, String w, String s, int c, ArrayList<Member> l)
		{
		id = num;
		name = n;
		webURL = w;
		sshURL = s;
		commitNumb = c;
		memberList = l;
		}

	/**
	 * Returns the id of a project
	 * @return the id to get
	 */
	public int getId ()
		{
		return this.id;
		}

	/**
	 * Sets a new id to a project
	 * @param id
	 *	the id to set
	 * @throws lyon1.iutinfo.cvda.exceptions.ProjectIdException
	 *	the id can't be negative or equal to zero
	 */
	public void setId (int id) throws ProjectIdException
		{
		if (id > 0)
			{
			this.id = id;
			}
		else
			{
			throw new ProjectIdException();
			}
		return;
		}

	/**
	 * Returns the name of a project
	 * @return the name to get
	 */
	public String getName ()
		{
		return this.name;
		}

	/**
	 * Sets a new name to a project
	 * @param name
	 *	the name to set
	 */
	public void setName (String name)
		{
		this.name = name;
		return;
		}

	/**
	 * Returns the web URL of a project
	 * @return the web URL to get
	 */
	public String getWebURL ()
		{
		return this.webURL;
		}

	/**
	 * Sets a new web URL to a project
	 * @param webURL
	 *	the webURL to set
	 * @throws lyon1.iutinfo.cvda.exceptions.ProjectWebURLException
	 *	the website URL must start with "https" and must end with ".git"
	 */
	public void setWebURL (String webURL) throws ProjectWebURLException
		{
		if ((webURL.substring(0, 5).equals("https")) && (webURL.substring(webURL.length() - 4, webURL.length()).equals(".git")))
			{
			this.webURL = webURL;
			}
		else
			{
			throw new ProjectWebURLException();
			}
		return;
		}

	/**
	 * Returns the SSH URL of a project
	 * @return the SSH URL to get
	 */
	public String getSshURL ()
		{
		return this.sshURL;
		}

	/**
	 * Sets a new SSH URL to a project
	 * @param sshURL
	 *	the SSH URL to set
	 */
	public void setSshURL (String sshURL)
		{
		this.sshURL = sshURL;
		return;
		}

	/**
	 * Returns the member list of a project
	 * @return the member list to get
	 */
	public ArrayList<Member> getMemberList ()
		{
		return this.memberList;
		}

	/**
	 * Sets a new member list to a project
	 * @param memberList
	 *	the member list to set
	 */
	public void setMemberList (ArrayList<Member> memberList)
		{
		this.memberList = memberList;
		return;
		}

	/**
	 * Returns the cumber of commits of a project
	 * @return the number of commits to get
	 */
	public int getCommitNumb ()
		{
		return this.commitNumb;
		}

	/**
	 * Sets a new number of commits to a project
	 * @param nbCommits
	 *	the number of commits to set
	 * @throws lyon1.iutinfo.cvda.exceptions.ProjectCommitNumberException
	 *	the number of commits can't be negative
	 */
	public void setCommitNumb (int nbCommits) throws ProjectCommitNumberException
		{
		if (nbCommits >= 0)
			{
			this.commitNumb = nbCommits;
			}
		else
			{
			throw new ProjectCommitNumberException();
			}
		return;
		}
	
	/**
	 * Converts all the information of a project into a String
	 * @return the project information
	 */
	@Override
	public String toString ()
		{
		String i = "";
		i += "Projet id #" + this.getId();
		i += "\nNom : \"" + this.getName() + "\"";
		i += "\nsshURL : \"" + this.getSshURL() + "\"";
		i += "\nwebURL : \"" + this.getWebURL() + "\"";
		i += "\nCommits : " + this.getCommitNumb();
		i += "\nMembres (" + this.getMemberList().size() + ")";
		for (Member m : this.getMemberList())
			{
			i += "\n\t#" + m.getId() + ": " + m.getName() + " (" + m.getProjectList().size() + " projets)";
			}
		return i;
		}

	/**
	 * Converts all the information of a project into an XML file, and returns the XML file content as a String
	 * @return the content of the XML file
	 */
	public String toXML ()
		{
		try
			{
			Document xml_doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
			
			// Root element "project"
			Element project = xml_doc.createElement("projet");
			project.setAttribute("id", String.valueOf(this.getId()));
			project.setAttribute("nbcommits", String.valueOf(this.getCommitNumb()));
			xml_doc.appendChild(project);
			
			// Child elements of root element
			Element name_element = xml_doc.createElement("nom");
			name_element.appendChild(xml_doc.createTextNode(this.getName()));
			project.appendChild(name_element);
			
			Element weburl_element = xml_doc.createElement("webURL");
			weburl_element.appendChild(xml_doc.createTextNode(this.getWebURL()));
			project.appendChild(weburl_element);
			
			Element sshurl_elem = xml_doc.createElement("sshURL");
			sshurl_elem.appendChild(xml_doc.createTextNode(this.getSshURL()));
			project.appendChild(sshurl_elem);
			
			Element member_elem = xml_doc.createElement("membres");
			project.appendChild(member_elem);
			
			// Child elements of members element
			for (Member m : this.getMemberList())
				{
				Element member = xml_doc.createElement("membre");
				member.setAttribute("id", String.valueOf(m.getId()));
				member_elem.appendChild(member);
				
				/// Child element of member element
				Element member_name = xml_doc.createElement("nom");
				member_name.appendChild(xml_doc.createTextNode(m.getName()));
				member.appendChild(member_name);
				
				Element member_nb_projects = xml_doc.createElement("nb-projets");
				member_nb_projects.appendChild(xml_doc.createTextNode(String.valueOf(m.getProjectList().size())));
				member.appendChild(member_nb_projects);
				}
			
			// This is to write the content of the xml_doc variable into a new XML file
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			DOMSource source = new DOMSource(xml_doc);
			StreamResult result = new StreamResult(new File("file.xml"));
			transformer.transform(source, result);
			
			// This is the return statement of the XML file as a String for testing
			StringWriter writer = new StringWriter();
			StreamResult result2 = new StreamResult(writer);
			transformer.transform(source, result2);
			String xml_String = writer.getBuffer().toString();
			return xml_String;
			}
		catch (ParserConfigurationException | TransformerException e)
			{
			System.out.println(e.getMessage());
			System.exit(0);
			}
		return null; // This return statement is necessary to make the code compilable
		}
	}
