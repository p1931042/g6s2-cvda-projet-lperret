/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.cvda.projet.models;

import java.io.File;
import java.io.StringWriter;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import lyon1.iutinfo.cvda.exceptions.MemberEmailException;
import lyon1.iutinfo.cvda.exceptions.MemberIdException;
import lyon1.iutinfo.cvda.exceptions.MemberNameException;
import lyon1.iutinfo.cvda.exceptions.MemberWebURLException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * <b>Member is the Java class that defines a member working on some projects.</b>
 * 
 * <p>A member has the following attributes :<p>
 * <ul>
 * <li>An identifier</li>
 * <li>A name</li>
 * <li>An email address</li>
 * <li>A website URL</li>
 * <li>A list of projects on which they are working</li>
 * </ul>
 * 
 * @author lowic
 * @version 1.0
 */
public class Member
	{
	/**
	 * The member's ID. It can be modified.
	 */
	private int id;
	/**
	 * The member's name. It can be modified.
	 */
	private String name;
	/**
	 * The member's email address. It can be modified.
	 */
	private String email;
	/**
	 * The member's website URL. It can be modified.
	 */
	private String website;
	/**
	 * The member's project list. It can be modified.
	 * <p>It does not need to be initialised in the constructor.</p>
	 */
	private ArrayList<Project> projectList;

	/**
	 * The first constructor of a Member object.
	 * <p>The project list is not specified.</p>
	 * @param num
	 *	the id to set
	 * @param n
	 *	the name to set
	 * @param e
	 *	the email address to set
	 * @param w
	 *	the website URL to set
	 */
	public Member (int num, String n, String e, String w)
		{
		id = num;
		name = n;
		email = e;
		website = w;
		projectList = new ArrayList();
		}

	/**
	 * The second constructor of a Member object.
	 * <p>The project list is specified.</p>
	 * @param num
	 *	the id to set
	 * @param n
	 *	the name to set
	 * @param e
	 *	the email address to set
	 * @param w
	 *	the website URL to set
	 * @param l
	 *	the project list to set
	 */
	public Member (int num, String n, String e, String w, ArrayList<Project> l)
		{
		id = num;
		name = n;
		email = e;
		website = w;
		projectList = l;
		}

	/**
	 * Returns the id of a member
	 * @return the id to get
	 */
	public int getId ()
		{
		return this.id;
		}

	/**
	 * Sets a new id to a member
	 * @param id
	 *	the id to set
	 * @throws lyon1.iutinfo.cvda.exceptions.MemberIdException
	 *	the id can't be negative or equal to zero
	 */
	public void setId (int id) throws MemberIdException
		{
		if (id > 0)
			{
			this.id = id;
			}
		else
			{
			throw new MemberIdException();
			}
		return;
		}

	/**
	 * Returns the name of a member
	 * @return the name to get
	 */
	public String getName ()
		{
		return this.name;
		}

	/**
	 * Sets a new name to a member
	 * @param name
	 *	the name to set
	 * @throws lyon1.iutinfo.cvda.exceptions.MemberNameException
	 *	the name can't be shorter than 2 characters
	 */
	public void setName (String name) throws MemberNameException
		{
		if (name.length() >= 2)
			{
			this.name = name;
			}
		else
			{
			throw new MemberNameException();
			}
		return;
		}

	/**
	 * Returns the email address of a member
	 * @return the email to get
	 */
	public String getEmail ()
		{
		return this.email;
		}

	/**
	 * Sets a new email address to a member
	 * @param email
	 *	the email address to set
	 * @throws lyon1.iutinfo.cvda.exceptions.MemberEmailException
	 *	the email must contain the @ character
	 */
	public void setEmail (String email) throws MemberEmailException
		{
		if (email.contains("@"))
			{
			this.email = email;
			}
		else
			{
			throw new MemberEmailException();
			}
		return;
		}

	/**
	 * Returns the website URL of a member
	 * @return the website URL to get
	 */
	public String getWebsite ()
		{
		return this.website;
		}

	/**
	 * Sets a new website URL to a member
	 * @param website
	 *	the website URL to set
	 * @throws lyon1.iutinfo.cvda.exceptions.MemberWebURLException
	 *	the website URL must start with "https" and must end with ".git"
	 */
	public void setWebsite (String website) throws MemberWebURLException
		{
		if ((website.substring(0, 5).equals("https")) && (website.substring(website.length() - 4, website.length()).equals(".git")))
			{
			this.website = website;
			}
		else
			{
			throw new MemberWebURLException();
			}
		return;
		}

	/**
	 * Returns the project list on which a member is working
	 * @return the project list to get
	 */
	public ArrayList<Project> getProjectList ()
		{
		return this.projectList;
		}

	/**
	 * Sets a new project list to a member
	 * @param projectList
	 *	the project list to set
	 */
	public void setProjectList (ArrayList<Project> projectList)
		{
		this.projectList = projectList;
		return;
		}

	/**
	 * Converts all the information of a member into a String
	 * @return the member's information
	 */
	@Override
	public String toString ()
		{
		String i = "";
		i += "Nom : " + this.getName();
		i += "Projet (" + this.getProjectList().size() + ")";
		for (Project p : this.projectList)
			{
			i += "\n\t#" + p.getId() + ": \"" + p.getName() + "\" (" + p.getMemberList().size() + " membres)";
			}
		return i;
		}

	/**
	 * Converts all the information of a member into an XML file, and returns the XML file content as a String
	 * @return the content of the XML file
	 */
	public String toXML ()
		{
		try
			{
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// Root element "member"
			Document doc = docBuilder.newDocument();
			Element member = doc.createElement("membre");
			doc.appendChild(member);
			member.setAttribute("id", String.valueOf(this.getId()));

			// Child elements of root element
			Element nom_Proj = doc.createElement("nom");
			member.appendChild(nom_Proj);
			nom_Proj.appendChild(doc.createTextNode(this.name));

			Element member_email = doc.createElement("email");
			member.appendChild(member_email);
			member_email.appendChild(doc.createTextNode(this.email));

			Element member_website = doc.createElement("website");
			member.appendChild(member_website);
			member_website.appendChild(doc.createTextNode(this.website));

			Element nb_of_projects = doc.createElement("nb-projets");
			member.appendChild(nb_of_projects);
			nb_of_projects.appendChild(doc.createTextNode(String.valueOf(this.projectList.size())));

			// This is to write the content of the doc variable into a new XML file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("file.xml"));
			transformer.transform(source, result);
			
			// This is the return statement of the XML file as a String for testing
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String xml_String = writer.getBuffer().toString();
			return xml_String;
			}
		catch (TransformerException | ParserConfigurationException e)
			{
			System.out.println(e.getMessage());
			System.exit(0);
			}
		return null; // This return statement is necessary to make the code compilable
		}
	}
