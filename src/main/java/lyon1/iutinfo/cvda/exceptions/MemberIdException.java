/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.cvda.exceptions;

/**
 *
 * @author lowic
 */
public class MemberIdException extends Exception
	{

	public MemberIdException ()
		{
		super("ID du membre invalide (nul ou négatif)");
		}
	}
