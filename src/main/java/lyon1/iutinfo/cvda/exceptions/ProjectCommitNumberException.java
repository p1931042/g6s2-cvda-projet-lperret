/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.cvda.exceptions;

/**
 *
 * @author lowic
 */
public class ProjectCommitNumberException extends Exception
	{

	public ProjectCommitNumberException ()
		{
		super("Nombre de commits du proket invalide (négatif)");
		}
	}
