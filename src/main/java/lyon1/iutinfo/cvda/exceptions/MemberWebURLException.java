/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.cvda.exceptions;

/**
 *
 * @author lowic
 */
public class MemberWebURLException extends Exception
	{

	public MemberWebURLException ()
		{
		super("URL web du membre invalide (ne commence pas par https ou ne finit pas par .git");
		}
	}
