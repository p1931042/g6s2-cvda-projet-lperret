# g6s2-cvda-projet-lperret  
  
PERRET Loïc  
loic.perret@etu.univ-lyon1.fr  
Je peux me débrouiller  
(https://g6s2-2020-iut-lyon1.framaboard.org/kanboard/?controller=ProjectOverviewController&action=show&project_id=12&search=status%3Aopen)  
(https://docs.kanboard.org/fr/latest/)  
(http://yuml.me/preview/f65e3aa6)  
(https://yuml.me/f65e3aa6.png)  
![Diagramme d'activité](https://yuml.me/f65e3aa6.png)  
  
(start)->(Je me connecte a la forge)->|b|  
|b|->(J'ai reussi a me connecter)->(Je telecharge mes projets)->|c|  
|c|->(J'ai reussi a telecharger mes projets)->(Je traduis en structure generique)->|d|  
|d|->(J'exporte en XML)->(end)  
|d|->(J'exporte en JSON)->(end)  
|d|->(J'exporte en HTML)->(end)  
|c|->(Je n'ai pas reussi car il n'y a pas de projet)->(Un message d'erreur s'affihe)->(end)  
|b|->(Je n'ai pas reussi a me connecter)->(Un message d'erreur s'affiche)->(end)  
  
Partie 3 en binôme avec :  
Gaëlle Bouchenot  
gaelle.bouchenot@etu.univ-lyon1.fr  
https://forge.univ-lyon1.fr/p1935539/g6s2-cvda-projet-gbouchenot  
  
Hash (projet) : ba5e849f12d5d7dfdfe086a78b9a67aec55d6deead0853e72657e93a  
Hash (membre) : 83d8fc14064833a74e3ac5529159dc95bb488c5d8b93d1c570f05dae  
  
La Javadoc du projet est générée dans g6s2-cvda-projet-lperret\target\site\apidocs  
C'est un fichier HTML.
